import Vue from 'vue'
import vuetify from './plugins/vuetify';
import Vuex from 'vuex'

import router from './router'
import store from './store'

import App from './App.vue'
import AppLayout from './components/layouts/AppLayout.vue'
import EmptyLayout from './components/layouts/EmptyLayout.vue'

Vue.config.productionTip = false

Vue.use(Vuex)
Vue.component('AppLayout', AppLayout)
Vue.component('EmptyLayout', EmptyLayout)

new Vue({
  router,
  vuetify,
  store,
  render: h => h(App)
}).$mount('#app')
