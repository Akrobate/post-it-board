import Vue from 'vue'
import Vuex from 'vuex'
import boardStore from './modules/boardStore'
import postItStore from './modules/postItStore'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    board: boardStore,
    postit: postItStore,
  },
})