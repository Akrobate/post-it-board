import { firebase } from '@/repositories'

const {
  board_collection,
} = firebase

const state = () => ({
  board_list: [],
})

const getters = {
  boardList: (_state) => _state.board_list,
}

const actions = {
  async createBoard({ commit }, board) {
    try {
      const data = await board_collection.add(board)
      console.log("created board", data)
      commit('add_board', board)
    } catch (error) {
      console.log("error on creation", error)
    }
  },
  setBoardList({ commit }, board_list) {
    commit('set_board_list', board_list)
  },
}

const mutations = {
  add_board(_state, data) {
    state.board_list = _state.board_list.concat(data)
  },
  set_board_list(_state, data) {
    _state.board_list = data
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
