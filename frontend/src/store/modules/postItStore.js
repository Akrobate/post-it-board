import { firebase } from '@/repositories'

const {
    post_it_collection,
} = firebase

const state = () => ({
  post_it_list: [],
  board_id: null,
})

const getters = {
  postItList: (_state) => _state.post_it_list,
}

const actions = {

  async createPostIt({ commit }, postit) {
    await post_it_collection.add(postit)
    commit('add_post_it', postit)
  },


  /**
   * @param {*} _
   * @param {*} param1
   * @returns {Pormise<*>}
   */
  updatePostIt(_, {
    postit_id,
    postit
  }) {
    return post_it_collection.doc(postit_id).update(postit);
  },

  setPostItList({ commit }, post_it_list) {
    commit('set_post_it_list', post_it_list)
  },

  setBoardId({ commit }, board_id) {
    commit('set_board_id', board_id)
  },

}

const mutations = {
  add_post_it(_state, data) {
    _state.post_it_list = _state.post_it_list.concat(data)
  },
  set_post_it_list(_state, data) {
    _state.post_it_list = data
  },
  set_board_id(_state, board_id) {
    _state.board_id = board_id
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
