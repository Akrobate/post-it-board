/* eslint-disable sort-keys */

'use strict';

const {
    MongoDbRepository,
} = require('./MongoDbRepository');

class PostItRepository {

    // eslint-disable-next-line require-jsdoc
    static get POST_IT_COLLECTION_NAME() {
        return 'post-it';
    }

    // eslint-disable-next-line require-jsdoc
    constructor(mongo_db_repository) {
        this.mongo_db_repository = mongo_db_repository;
    }

    /* istanbul ignore next */
    // eslint-disable-next-line require-jsdoc
    static getInstance() {
        if (PostItRepository.instance === null) {
            PostItRepository.instance = new PostItRepository(
                MongoDbRepository.getInstance()
            );
        }
        return PostItRepository.instance;
    }

    /**
     * @param {Object} criteria
     * @returns {Promise}
     */
    search(criteria) {
        const {
            limit,
            offset,
        } = criteria;

        const query = this.formatSearchCriteria(criteria);

        return this
            .mongo_db_repository
            .findDocumentList(
                PostItRepository.POST_IT_COLLECTION_NAME,
                query,
                limit,
                offset,
                undefined,
                {
                    pubDate: -1,
                }
            );
    }


    /**
     *
     * @param {Object} criteria
     * @returns {Object}
     */
    formatSearchCriteria(criteria) {

        const {
            company_id_list,
        } = criteria;

        const query = {};

        if (company_id_list) {
            query.rss_feed_url_id = Object.assign({}, query.rss_feed_url_id, {
                $in: company_id_list,
            });
        }

        return query;
    }

    /**
     * While script is running it checks for existence of
     * items by guid and rss_feed_url_id
     * This method creates the multi index on this two fields
     * @return {Promise}
     */
    createIndexForExistanceCheck() {
        return this.mongo_db_repository
            .createIndex(
                PostItRepository.POST_IT_COLLECTION_NAME,
                {
                    guid: 1,
                    rss_feed_url_id: 1,
                }
            );
    }

    /**
     * @returns {Promise}
     */
    closeConnection() {
        return this.mongo_db_repository.closeConnection();
    }

}

PostItRepository.instance = null;

module.exports = {
    PostItRepository,
};
