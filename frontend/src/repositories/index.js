'use strict';

const {
    MongoDbRepository,
} = require('./MongoDbRepository');
const {
    BoardRepository,
} = require('./BoardRepository');
const {
    PostItRepository,
} = require('./BoardRepository');

module.exports = {
    MongoDbRepository,
    BoardRepository,
};
