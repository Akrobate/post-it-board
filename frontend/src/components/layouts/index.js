'use strict'

import AppLayout from './AppLayout'
import EmptyLayout from './EmptyLayout'

export {
    AppLayout,
    EmptyLayout,
}
