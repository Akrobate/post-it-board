'use strict'

import BoardPage from './BoardPage'
import HomePage from './HomePage'
import AboutPage from './AboutPage'

export {
    BoardPage,
    HomePage,
    AboutPage,
}
