import Vue from 'vue'
import Router from 'vue-router'

import {
    BoardPage,
    HomePage,
    AboutPage,
} from '@/components/pages'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'HomePage',
      component: HomePage,
      props: true,
      meta: {
        layout: 'AppLayout',
      }
    },
    {
      path: '/about',
      name: 'AboutPage',
      component: AboutPage,
      props: true,
      meta: {
        layout: 'AppLayout',
      }
    },
    {
      path: '/board/:board_id',
      name: 'BoardPage',
      component: BoardPage,
      props: true,
      meta: {
        layout: 'EmptyLayout',
      }
    },
  ],
  
})

export default router
