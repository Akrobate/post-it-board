# post-it-board

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## Api definitions

POST /boards

body

```json
{
    name: String
}
```

return

```json
{
    id: Number
    url_code: String
}
```

GET boards/{url_code}

```json
{
    id: Number,
    url_code: String,
    postit_list: [
        {
            id: Number,
            content: String,
            bgcolor: String,
            color: String,
            x: Float,
            y: Float,
        }
    ]
}
```



## Task list

* *26/09/22* Add configuration for MongoDb Repository
* *26/09/22* Prepare docker-compose

