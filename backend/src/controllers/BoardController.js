'use strict';

const HTTP_CODE = require('http-status');

const {
    BoardService,
} = require('../services');

class BoardController {

    /**
     * @param {BoardService} board_service
     */
    constructor(board_service) {
        this.board_service = board_service;
    }


    /* istanbul ignore next */
    // eslint-disable-next-line require-jsdoc
    static getInstance() {
        if (BoardController.instance === null) {
            BoardController.instance = new BoardController(
                BoardService.getInstance()
            );
        }
        return BoardController.instance;
    }


    /**
     * @param {Object} request
     * @param {Object} response
     * @returns {Void}
     */
    async read(request, response) {
        const {
            id,
        } = request.params;
        const board = await this.board_service.read(id);
        response.status(HTTP_CODE.OK).json(board);
    }


    /**
     * @param {Object} request
     * @param {Object} response
     * @returns {Void}
     */
    async search(request, response) {
        const {
            name,
        } = request.query;
        const board_list = await this.board_service.search({
            name,
        });
        response.status(HTTP_CODE.OK).json(board_list);
    }


    /**
     * @param {Object} request
     * @param {Object} response
     * @returns {Void}
     */
    async create(request, response) {
        const {
            body,
        } = request;
        const board = await this.board_service.create(body);
        response.status(HTTP_CODE.CREATED).json(board);
    }


    /**
     * @param {Object} request
     * @param {Object} response
     * @returns {Void}
     */
    async update(request, response) {
        const {
            id,
        } = request.params;
        const {
            body,
        } = request;
        const board = await this.board_service.update({
            ...body,
            id,
        });
        response.status(HTTP_CODE.CREATED).json(board);
    }


    /**
     * @param {Object} request
     * @param {Object} response
     * @returns {Void}
     */
    async delete(request, response) {
        const {
            id,
        } = request.params;
        await this.board_service.delete(id);
        response.status(HTTP_CODE.OK).json({});
    }


    /**
     * @param {Object} request
     * @param {Object} response
     * @returns {Void}
     */
    async createPostIt(request, response) {
        const {
            body,
        } = request;
        const {
            board_id,
        } = request.params;
        const post_it = await this.board_service.createPostIt({
            ...body,
            board_id,
        });
        response.status(HTTP_CODE.CREATED).json(post_it);
    }


    /**
     * @param {Object} request
     * @param {Object} response
     * @returns {Void}
     */
    async searchPostIt(request, response) {
        const {
            query,
        } = request;
        const {
            board_id,
        } = request.params;
        const post_it_list = await this.board_service.searchPostIt({
            ...query,
            board_id,
        });
        response.status(HTTP_CODE.OK).json(post_it_list);
    }


    /**
     * @param {Object} request
     * @param {Object} response
     * @returns {Void}
     */
    async readPostIt(request, response) {
        const {
            id,
        } = request.params;
        const post_it = await this.board_service.readPostIt(id);
        response.status(HTTP_CODE.OK).json(post_it);
    }


    /**
     * // @todo
     * @param {Object} request
     * @param {Object} response
     * @returns {Void}
     */
    async deletePostIt(request, response) {
        const {
            id,
        } = request.params;
        await this.board_service.deletePostIt(id);
        response.status(HTTP_CODE.OK).json({});
    }

}

BoardController.instance = null;

module.exports = {
    BoardController,
};
