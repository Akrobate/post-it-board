/* eslint-disable sort-keys */

'use strict';

const {
    MongoDbRepository,
} = require('./MongoDbRepository');

const {
    AbstractMongoDbRepository,
} = require('./AbstractMongoDbRepository');

class BoardRepository extends AbstractMongoDbRepository {

    // eslint-disable-next-line require-jsdoc
    static get COLLECTION_NAME() {
        return 'boards';
    }


    /* istanbul ignore next */
    // eslint-disable-next-line require-jsdoc
    static getInstance() {
        if (BoardRepository.instance === null) {
            BoardRepository.instance = new BoardRepository(
                MongoDbRepository.getInstance()
            );
        }
        return BoardRepository.instance;
    }


    /**
     * @param {Object} criteria
     * @returns {Promise}
     */
    search(criteria) {
        const {
            limit,
            offset,
        } = criteria;

        const query = this.formatSearchCriteria(criteria);

        return super
            .search(
                query,
                {},
                limit,
                offset
            );
    }


    /**
     *
     * @param {Object} criteria
     * @returns {Object}
     */
    formatSearchCriteria(criteria) {

        const {
            board_id_list,
            name,
        } = criteria;

        const query = {};

        if (board_id_list) {
            query.board_id = {
                $in: board_id_list,
            };
        }

        if (name) {
            query.name = {
                $eq: name,
            };
        }

        return query;
    }


    /**
     *
     * @param {Object} document
     * @returns {Promise<Object|error>}
     */
    async create(document) {
        const {
            insertedId,
        } = await super.create(document);
        return this.read(insertedId);
    }


    /**
     *
     * @param {Object} document
     * @returns {Promise<Object|error>}
     */
    async update(id, data) {
        await super.update(id, data);
        return this.read(id);
    }
}

BoardRepository.instance = null;

module.exports = {
    BoardRepository,
};
