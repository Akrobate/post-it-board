/* eslint-disable sort-keys */

'use strict';

const {
    MongoDbRepository,
} = require('./MongoDbRepository');

const {
    AbstractMongoDbRepository,
} = require('./AbstractMongoDbRepository');

class PostItRepository extends AbstractMongoDbRepository {

    // eslint-disable-next-line require-jsdoc
    static get COLLECTION_NAME() {
        return 'post-its';
    }


    /* istanbul ignore next */
    // eslint-disable-next-line require-jsdoc
    static getInstance() {
        if (PostItRepository.instance === null) {
            PostItRepository.instance = new PostItRepository(
                MongoDbRepository.getInstance()
            );
        }
        return PostItRepository.instance;
    }


    /**
     * @param {Object} criteria
     * @returns {Promise}
     */
    search(criteria) {
        const {
            limit,
            offset,
        } = criteria;

        const query = this.formatSearchCriteria(criteria);

        return super
            .search(
                query,
                {},
                limit,
                offset
            );
    }


    /**
     *
     * @param {Object} criteria
     * @returns {Object}
     */
    formatSearchCriteria(criteria) {
        const {
            id_list,
            name,
        } = criteria;

        const query = {};

        if (id_list) {
            query.id = {
                $in: id_list,
            };
        }

        if (name) {
            query.name = {
                $eq: name,
            };
        }
        
        return query;
    }


    /**
     *
     * @param {Object} document
     * @returns {Promise<Object|error>}
     */
    async create(document) {
        const {
            insertedId,
        } = await super.create(document);
        return this.read(insertedId);
    }

}

PostItRepository.instance = null;

module.exports = {
    PostItRepository,
};
