'use strict';

const {
    MongoDbRepository,
} = require('./MongoDbRepository');
const {
    BoardRepository,
} = require('./BoardRepository');
const {
    PostItRepository,
} = require('./PostItRepository');
const {
    AbstractMongoDbRepository,
} = require('./AbstractMongoDbRepository');


module.exports = {
    MongoDbRepository,
    BoardRepository,
    PostItRepository,
    AbstractMongoDbRepository,
};
