'use strict';

const {
    logger,
} = require('../logger');


class AbstractMongoDbRepository {

    /**
     * @static
     */
    static get COLLECTION_NAME() {
        throw new Error('COLLECTION_NAME must be overriden in child class');
    }

    /**
     * @param {MongoDbRepository} mongodb_db_repository
     */
    constructor(
        mongodb_db_repository
    ) {
        this.mongodb_db_repository = mongodb_db_repository;
    }

    /**
     * @returns {Promise<Object|error>}
     */
    getConnection() {
        return this
            .mongodb_db_repository
            .getConnection();
    }

    /**
     * @returns {Promise<Object|error>}
     */
    closeConnection() {
        return this
            .mongodb_db_repository.closeConnection();
    }


    /**
     * @param {String} id
     * @returns {Promise<Object|error>}
     */
    read(id) {
        return this
            .mongodb_db_repository
            .findDocument(
                this.constructor.COLLECTION_NAME,
                {
                    _id: this.mongodb_db_repository.builObjectIdFromString(id),
                }
            );
    }


    /**
     *
     * @param {Object} query
     * @returns {Promise<Object|error>}
     */
    find(query) {
        return this
            .mongodb_db_repository
            .findDocument(
                this.constructor.COLLECTION_NAME,
                query
            );
    }

    /**
     *
     * @param {Object} query
     * @param {Object} sort
     * @param {Number} limit
     * @param {Number} offset
     * @param {Object} fields_selection
     * @returns {Promise<Object|error>}
     */
    search(query, sort, limit, offset, fields_selection) {
        return this
            .mongodb_db_repository
            .findDocumentList(
                this.constructor.COLLECTION_NAME,
                query,
                limit,
                offset,
                fields_selection,
                sort
            );
    }

    /**
     *
     * @param {String} id
     * @param {Object} document
     * @returns {Promise<Object|error>}
     */
    update(id, document) {
        return this
            .mongodb_db_repository
            .updateDocument(
                this.constructor.COLLECTION_NAME,
                {
                    _id: this.mongodb_db_repository
                        .builObjectIdFromString(id),
                },
                document
            );
    }


    /**
     *
     * @param {Object} query
     * @param {Object} document
     * @returns {Promise<Object|error>}
     */
    updateMany(query, document) {
        return this
            .mongodb_db_repository
            .updateDocuments(
                this.constructor.COLLECTION_NAME,
                query,
                document
            );
    }


    /**
     *
     * @param {Object} document
     * @returns {Promise<Object|error>}
     */
    create(document) {
        return this
            .mongodb_db_repository
            .insertDocument(
                this.constructor.COLLECTION_NAME,
                document
            )
            .catch(logger.log);
    }


    /**
     * @param {String} technical_name
     * @returns {Promise}
     */
    async delete(id) {
        const result = await this
            .mongodb_db_repository
            .deleteDocument(
                this.constructor.COLLECTION_NAME,
                {
                    _id: this.mongodb_db_repository
                        .builObjectIdFromString(id),                
                }
            );
        if (result.deletedCount === 0) {
            throw new Error('NOT_FOUND');
        }
        return result;
    }
    /**
     *
     * @param {Number} index
     * @param {Object} criteria
     * @returns {Promise<Object|error>}
     */
    getDocumentAtIndex(index, criteria) {
        return this
            .mongodb_db_repository
            .findDocumentList(
                this.constructor.COLLECTION_NAME,
                this.formatCriteria(criteria),
                undefined,
                1,
                index
            );
    }


    /**
     * @param {Object} criteria
     * @returns {Promise}
     */
    count(criteria) {
        return this
            .mongodb_db_repository
            .countDocuments(
                this.constructor.COLLECTION_NAME,
                this.formatCriteria(criteria)
            );
    }


    /**
     * @returns {Promise}
     */
    truncate() {
        return this
            .mongodb_db_repository
            .truncate(
                this.constructor.COLLECTION_NAME
            );
    }


    /**
     * Utilitary method
     * formats the query to criteria
     * @param {Object|undefined} criteria
     * @returns {Object}
     */
    formatCriteria(criteria) {
        if (criteria === undefined) {
            return {};
        }
        return criteria;
    }
}

AbstractMongoDbRepository.instance = null;

module.exports = {
    AbstractMongoDbRepository,
};
