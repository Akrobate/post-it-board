'use strict';

const express = require('express');
const router = require('./router');
const {
    error_manager_middleware,
    not_found_error_middleware,
} = require('./middlewares/HttpErrorManager');

const app = express();

app.use(express.json());
app.use('/v1', router);

app.use(not_found_error_middleware);
app.use(error_manager_middleware);

module.exports = app;
