'use strict';

const {
    configuration,
} = require('./configuration');
const {
    logger,
} = require('./logger');

const app = require('./app');

app.listen(configuration.server.port, () => {
    logger.log(`Server running on ${configuration.server.port}`);
});
