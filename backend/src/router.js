'use strict';

const {
    Router,
} = require('express');

const {
    BoardController,
} = require('./controllers');

const router = new Router();
const board_controller = BoardController.getInstance();

router.get('/ping', (request, result) => {
    result
        .status(200)
        .json({
            status: 'ok',
        });
});


router.get(
    '/boards/:id',
    (request, result, next) => board_controller
        .read(request, result)
        .catch(next)
);


router.post(
    '/boards',
    (request, result, next) => board_controller
        .create(request, result)
        .catch(next)
);

router.get(
    '/boards',
    (request, result, next) => board_controller
        .search(request, result)
        .catch(next)
);

router.patch(
    '/boards/:id',
    (request, result, next) => board_controller
        .update(request, result)
        .catch(next)
);


router.delete(
    '/boards/:id',
    (request, result, next) => board_controller
        .delete(request, result)
        .catch(next)
);


router.post(
    '/boards/:board_id/post-its',
    (request, result, next) => board_controller
        .createPostIt(request, result)
        .catch(next)
);

router.get(
    '/boards/:board_id/post-its',
    (request, result, next) => board_controller
        .searchPostIt(request, result)
        .catch(next)
);

router.get(
    '/boards/:board_id/post-its/:id',
    (request, result, next) => board_controller
        .readPostIt(request, result)
        .catch(next)
);

router.delete(
    '/boards/:board_id/post-its/:id',
    (request, result, next) => board_controller
        .deletePostIt(request, result)
        .catch(next)
);

module.exports = router;
