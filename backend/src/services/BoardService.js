'use strict';

const {
    CustomError,
} = require('../CustomError');

const {
    BoardRepository,
    PostItRepository,
} = require('../repositories');

class BoardService {


    /**
     * @param {BoardRepository} board_repository
     * @param {PostItRepository} post_it_repository
     */
    constructor(board_repository, post_it_repository) {
        this.board_repository = board_repository;
        this.post_it_repository = post_it_repository;
    }


    /* istanbul ignore next */
    // eslint-disable-next-line require-jsdoc
    static getInstance() {
        if (BoardService.instance === null) {
            BoardService.instance = new BoardService(
                BoardRepository.getInstance(),
                PostItRepository.getInstance()
            );
        }
        return BoardService.instance;
    }


    /**
     * @param {Object} criteria
     * @returns {Object}
     */
    search(criteria) {
        return this.board_repository.search(criteria);
    }


    /**
     * @param {String} id
     * @returns {Object}
     */
    read(id) {
        const board = this.board_repository.read(id);
        if (board === null) {
            throw new CustomError(CustomError.NOT_FOUND);
        }
        return board;
    }


    /**
     * @param {Obejct} data
     * @returns {Object}
     */
    create(data) {
        return this.board_repository.create(data);
    }


    /**
     * @param {Object} data
     * @returns {Object}
     */
    update(data) {
        const {
            id,
        } = data;
        return this.board_repository.update(id, data);
    }


    /**
     * @param {String} id
     * @returns {Object}
     */
    delete(id) {
        return this.board_repository.delete(id);
    }


    /**
     * @param {String} id
     * @returns {Object}
     */
    async readPostIt(id) {
        const postit = await this.post_it_repository.read(id);
        if (postit === null) {
            throw new CustomError(CustomError.NOT_FOUND);
        }
        return postit;
    }

    /**
     * @param {Object} criteria
     * @returns {Array<Object>}
     */
    searchPostIt(criteria) {
        return this.post_it_repository.search(criteria);
    }


    /**
     * @param {Obejct} data
     * @returns {Object}
     */
    createPostIt(data) {
        return this.post_it_repository.create(data);
    }


    /**
     * @param {String} id
     * @returns {Object}
     */
    deletePostIt(id) {
        return this.post_it_repository.delete(id);
    }

}

BoardService.instance = null;

module.exports = {
    BoardService,
};
