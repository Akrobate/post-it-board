'use strict';

const {
    PostItRepository,
} = require('../../src/repositories');
const {
    DataSeeder,
} = require('../test_helpers');
const {
    expect,
} = require('chai');
const {
    post_it_1,
    post_it_2,
} = require('../test_seeds');

const created_seeds = {};

const post_it_repository = PostItRepository.getInstance();

describe('PostItRepository', () => {

    beforeEach(async () => {
        await DataSeeder.truncate('PostItRepository');
        created_seeds.post_it_1 = await DataSeeder.create('PostItRepository', post_it_1);
        created_seeds.post_it_2 = await DataSeeder.create('PostItRepository', post_it_2);
    });


    it('PostItRepository create', async () => {
        const post_it_creation = {
            name: 'Post it Creation test',
        };
        const result = await post_it_repository.create(post_it_creation);
        expect(result).to.have.property('name', post_it_creation.name);
    });


    it('PostItRepository search', async () => {
        const post_list = await post_it_repository.search({
            name: post_it_2.name,
        });

        expect(post_list).to.be.an('Array');
        expect(post_list.length).to.equal(1);
        const [
            first_item,
        ] = post_list;
        expect(first_item).to.have.property('name', post_it_2.name);
    });

});
