'use strict';

const {
    BoardRepository,
} = require('../../src/repositories');
const {
    DataSeeder,
} = require('../test_helpers');
const {
    expect,
} = require('chai');

const {
    board_1,
    board_2,
} = require('../test_seeds');

const created_seeds = {};

const board_repository = BoardRepository.getInstance();

describe('BoardRepository', () => {

    beforeEach(async () => {
        await DataSeeder.truncate('BoardRepository');
        created_seeds.board_1 = await DataSeeder.create('BoardRepository', board_1);
        created_seeds.board_2 = await DataSeeder.create('BoardRepository', board_2);
    });

    it('BoardRepository create', async () => {
        const board_creation = {
            name: 'Board Creation test',
        };
        const result = await board_repository.create(board_creation);
        expect(result).to.have.property('name', board_creation.name);
    });


    it('BoardRepository search', async () => {
        const board_list = await board_repository.search({
            name: board_2.name,
            limit: 10,
            offset: 0,
        });

        expect(board_list).to.be.an('Array');
        expect(board_list.length).to.equal(1);
        const [
            first_item,
        ] = board_list;
        expect(first_item).to.have.property('name', board_2.name);
    });

});
