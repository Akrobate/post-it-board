'use strict';

const superTest = require('supertest');
const HTTP_CODE = require('http-status');
const {
    expect,
} = require('chai');
const {
    DataSeeder,
} = require('../test_helpers');

const app = require('../../src/app');

const superApp = superTest(app);
const {
    board_1,
    board_2,
} = require('../test_seeds');

const created_seeds = {};

describe('Board', () => {

    beforeEach(async () => {
        await DataSeeder.truncate('BoardRepository');
        created_seeds.board_1 = await DataSeeder.create('BoardRepository', board_1);
        created_seeds.board_2 = await DataSeeder.create('BoardRepository', board_2);
    });


    it('Should be able to create a board', async () => {
        await superApp
            .post('/v1/boards')
            .send({
                name: 'Title of board',
            })
            .expect(HTTP_CODE.CREATED)
            .expect((response) => {
                expect(response).to.have.property('body');
                expect(response.body).to.have.property('name', 'Title of board');
            });
    });


    it('Should be able to search a board', async () => {
        await superApp
            .get('/v1/boards')
            .query({
                name: board_2.name,
            })
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                const {
                    body,
                } = response;
                const [
                    found_board,
                ] = body;
                expect(found_board.name).to.equal(board_2.name);
            });
    });


    it('Should be able to update a board by id', async () => {

        const name_to_update = 'Updated title of board';

        await superApp
            .patch(`/v1/boards/${created_seeds.board_2._id}`)
            .send({
                name: name_to_update,
            })
            .expect(HTTP_CODE.CREATED)
            .expect((response) => {
                expect(response.body.name).to.equal(name_to_update);
            });

        await superApp
            .get(`/v1/boards/${created_seeds.board_2._id}`)
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response.body.name).to.equal(name_to_update);
            });
    });


    it('Should be able to get a board by id', async () => {
        await superApp
            .get(`/v1/boards/${created_seeds.board_2._id}`)
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                expect(response.body.name).to.equal(board_2.name);
            });
    });


    it('Should be able to delete a board by id', async () => {
        await superApp
            .delete(`/v1/boards/${created_seeds.board_2._id}`)
            .expect(HTTP_CODE.OK);

        await superApp
            .get(`/v1/boards/${created_seeds.board_2._id}`)
            .expect((response) => expect(response.body).to.equal(null));
    });

});
