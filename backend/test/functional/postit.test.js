'use strict';

const superTest = require('supertest');
const HTTP_CODE = require('http-status');
const {
    expect,
} = require('chai');
const {
    DataSeeder,
} = require('../test_helpers');

const app = require('../../src/app');

const superApp = superTest(app);
const {
    post_it_1,
    post_it_2,
} = require('../test_seeds');


const created_seeds = {};

describe('PostIt', () => {

    beforeEach(async () => {
        await DataSeeder.truncate('PostItRepository');
        created_seeds.post_it_1 = await DataSeeder.create('PostItRepository', post_it_1);
        created_seeds.post_it_2 = await DataSeeder.create('PostItRepository', post_it_2);
    });


    it('Should be able to create a post_it', async () => {
        const random_board_id = 'RANDOM_BOARD_ID';
        await superApp
            .post(`/v1/boards/${random_board_id}/post-its`)
            .send({
                name: 'Title of post_it',
            })
            .expect(HTTP_CODE.CREATED)
            .expect((response) => {
                expect(response).to.have.property('body');
                expect(response.body).to.have.property('name', 'Title of post_it');
                expect(response.body).to.have.property('board_id', random_board_id);
            });
    });


    describe('Search', () => {
        it('Should be able to search a post_it', async () => {
            const random_board_id = 'RANDOM_BOARD_ID';
            await superApp
                .get(`/v1/boards/${random_board_id}/post-its`)
                .query({
                    name: post_it_2.name,
                })
                .expect(HTTP_CODE.OK);
        });
    });


    describe('Read', () => {
        it('Should be able to read a post_it', async () => {
            const random_board_id = 'RANDOM_BOARD_ID';
            await superApp
                .get(`/v1/boards/${random_board_id}/post-its/${created_seeds.post_it_1._id}`)
                .expect(HTTP_CODE.OK)
                .expect((response) => {
                    expect(response).to.have.property('body');
                    expect(response.body).to.have.property('name', created_seeds.post_it_1.name);
                });
        });
    });


    describe('Delete', () => {
        it('Should be able to delete a post_it', async () => {
            const random_board_id = 'RANDOM_BOARD_ID';
            await superApp
                .delete(`/v1/boards/${random_board_id}/post-its/${created_seeds.post_it_1._id}`)
                .expect(HTTP_CODE.OK)
                .expect((response) => {
                    expect(response).to.have.property('body');
                    expect(response.body).to.be.an('Object');
                });

            await superApp
                .get(`/v1/boards/${random_board_id}/post-its/${created_seeds.post_it_1._id}`)
                .expect(HTTP_CODE.NOT_FOUND);
        });
    });
});
