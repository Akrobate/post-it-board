'use strict';

const superTest = require('supertest');
const HTTP_CODE = require('http-status');

const {
    expect,
} = require('chai');

const app = require('../src/app');

const superApp = superTest(app);

describe('Ping', () => {
    it('Should be able ping server', async () => {
        await superApp
            .get('/v1/ping')
            .expect(HTTP_CODE.OK)
            .expect((response) => {
                const {
                    body,
                } = response;
                expect(body).to.have.property('status', 'ok');
            });
    });
});
