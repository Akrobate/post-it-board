'use strict';

const {
    board_1,
    board_2,
    post_it_1,
    post_it_2,
} = require('./test_data_seeds');

module.exports = {
    board_1,
    board_2,
    post_it_1,
    post_it_2,
};
