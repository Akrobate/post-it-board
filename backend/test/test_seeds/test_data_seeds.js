'use strict';

const board_1 = {
    name: 'First board seed',
};
const board_2 = {
    name: 'Second board seed',
};

const post_it_1 = {
    name: 'First postit seed',
};

const post_it_2 = {
    name: 'Second postit seed',
};

module.exports = {
    board_1,
    board_2,
    post_it_1,
    post_it_2,
};
