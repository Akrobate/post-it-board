'use strict';

class DataSeeder {


    /**
     * @returns {Promise}
     */
    static truncateAll() {
        return DataSeeder.truncateList([
        ]);
    }


    /**
     * @param {Array<String>} repository_name_list
     * @returns {Promise}
     */
    static truncateList(repository_name_list) {
        return Promise.all(
            repository_name_list
                .map((repository_name) => DataSeeder.truncate(repository_name))
        );
    }


    /**
     * @param {String} repository_name
     * @returns {Promise}
     */
    static truncate(repository_name) {
        const repository = DataSeeder.loadRepository(repository_name);
        return repository.truncate();
    }


    /**
     * @param {String} repository_name
     * @param {Object} input
     * @returns {Promise}
     */
    static create(repository_name, input) {
        const repository = DataSeeder.loadRepository(repository_name);
        return repository.create(input);
    }


    /**
     * @param {String} repository_name
     * @returns {Object}
     */
    static loadRepository(repository_name) {
        // eslint-disable-next-line global-require
        return require(`../../src/repositories/${repository_name}`)[repository_name].getInstance();
    }
}


module.exports = {
    DataSeeder,
};
