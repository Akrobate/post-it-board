'use strict';

const {
    DataSeeder,
} = require('./DataSeeder');

module.exports = {
    DataSeeder,
};
