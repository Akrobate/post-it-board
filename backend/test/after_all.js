'use strict';

const {
    MongoDbRepository,
} = require('../src/repositories');

const mongo_db_repository = MongoDbRepository.getInstance();

after(async () => {
    await mongo_db_repository.closeConnection();
});
